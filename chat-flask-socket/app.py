from flask import Flask, render_template, request
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)
app.config["SECRET_KEY"] = "secret"
socketio = SocketIO(app, async_mode='threading')


@app.route('/')
def hello_world():
    title = "Faiber"
    return render_template("principal/index.html", title=title)

@socketio.on("connect")
def handlerStart():
    print("inicia socket")
    print(request.namespace);


@socketio.on("chat:message")
def handlerMessage(data):
    emit("chat:message", data, broadcast=True)

@socketio.on("chat:typing")
def handlerTyping(data):
    emit("chat:typing", data, broadcast=True, include_self=False)

if __name__ == '__main__':
    socketio.run(app)
