// capturo el io que se vuelve global al vincular el script socket.io.js
const socket = io('http://localhost:3000')

let message = document.getElementById("message");
let username = document.getElementById("username");
let btn = document.getElementById("send");
let output = document.getElementById("output");
let actions = document.getElementById("actions");

btn.addEventListener("click", function(){

    username.setAttribute("disabled","");

    let data = {
        username: username.value,
        message: message.value
    }
    // el nombre chat:message puede ser cualquiera solo es para identificar
    socket.emit('chat:message', data);
});


message.addEventListener("keypress", function(){
    socket.emit('chat:typing', username.value);
});

socket.on("chat:message_server", function(data){
    actions.innerHTML = "";
    let clase = "left";
    if(data.username === username.value){
        clase = "right";
    }

    output.innerHTML += `
        <p class="${clase}">
            <strong>${data.username}</strong>:
            ${data.message}
        </p>
    `;
});
socket.on("chat:typing_server", function(data){
    actions.innerHTML = `
        <p class="center"><em>${data} is typing a message</em></p>
    `
});