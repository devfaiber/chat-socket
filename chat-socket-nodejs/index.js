"use strict"
const express = require("express");
const app = express();
const path = require("path");

const SocketIo = require("socket.io");

app.set("port",process.env.PORT || 3000); // define una propiedad

//defino los estaticos
app.use(express.static(path.join(__dirname,"public")))

const server = app.listen(app.get("port"),()=>{
    console.log("corriendo express en puerto", app.get("port"));
});


const io = SocketIo.listen(server); // paso el servidor inicializado a socket.io


// cuando se conecte
io.on("connection", (socket)=>{
    console.log("socket.io inicializado: ",socket.id);

    // ejecuta el evento que emitimos en chat.js
    socket.on("chat:message", (data)=>{
        console.log(data)
        // emitir a todos los sockets
        io.sockets.emit("chat:message_server", data);
    })
    // otro evento emitido
    socket.on("chat:typing", (data)=>{
        // emitir a todos menos al que lo envio
        socket.broadcast.emit("chat:typing_server", data);
    })

})




